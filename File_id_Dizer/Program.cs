﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.IO;

namespace File_id_Dizer
{
    class Program
    {
        public static String fullPath;
        public static bool applyToAll;
        public static String applyTag;
        public static String fileMask;
        public static bool overWriteWhenCopy;

        static void Main(string[] args)
        {
            fullPath = System.Environment.CurrentDirectory;
            applyToAll = false;
            applyTag = String.Empty;
            fileMask = "*.*";
            overWriteWhenCopy = false;

            if (args.Length > 0)
            {
                String argsString = String.Join("", args);
                String[] splitBySlash = argsString.Split('/');

                //Обработка входных параметров
                foreach (String item in splitBySlash)
                {
                    if (item.Trim().Length == 0) continue;

                    if (item.Trim().Equals("?"))
                    {
                        Console.WriteLine("--- HELP PAGE ---");
                        Console.WriteLine("Possible command line parameters:\n/path=pathToDir -- Path to the target directory. If not set, uses current directory.\nExample:\nFile_id_Dizer.exe /path=C:\\TESTDIR\n\n");
                        Console.WriteLine("/applyToAll=applyTag -- applies specified tag to all files in working directory.\nExample:\nFile_id_Dizer.exe /path=C:\\TESTDIR /applytoall=\"common tag for all\"\n\n");
                        Console.WriteLine("/fileMask=mask -- mask for files to work with. If not specified, work with all (*.*) files.\nExample:\nFile_id_Dizer.exe /path=C:\\TESTDIR /applytoall=\"common tag for all\" /filemask=*.jpg\n\n");
                        Console.WriteLine("/overWrite=bool -- whether overwrite or not files in target tag subdirectories when copying. If not set, equals to false.\nExample:\nFile_id_Dizer.exe /path=C:\\TESTDIR /applytoall=\"common tag for all\" /filemask=*.jpg /overwrite=true");
                        Console.WriteLine("--- HELP PAGE END ---");

                        return;
                    }

                    if (!item.Contains("="))
                    {
                        Console.WriteLine("parameter <" + item + "> doesn't contains '=' symbol! Skipped...");
                        continue;
                    }

                    String paramName = item.Split('=')[0].Trim().ToLower();
                    String paramValue = item.Split('=')[1].Trim();

                    switch (paramName)
                    {
                        case "path":
                            fullPath = paramValue;
                            break;

                        case "applytoall":
                            applyToAll = true;
                            applyTag = paramValue;
                            break;
                        case "filemask":
                            fileMask = paramValue;
                            break;
                        case "overwrite":
                            if (paramValue.Trim().ToLower().Equals("true")) overWriteWhenCopy = true;
                            break;
                        default:
                            break;
                    }
                }
            }

            if (!System.IO.Directory.Exists(fullPath))
            {
                Console.WriteLine("path <" + fullPath + "> doesn't exists! Progam terminates...");
                return;
            }

            //--- Основная часть - обработка файлов в заданном (или текущем, если не задан) каталоге
            //Открываем description на чтение, считываем всё в набор key-value
            Dictionary<String, String> existedDescr = GetCurrentDescr(fullPath);

            //Перебираем заданный каталог по одному файлу
            String[] fileNamesArray = Directory.GetFiles(fullPath, fileMask);
            foreach (String fileName in fileNamesArray)
            {
                String shortFileName = Path.GetFileName(fileName);
                if (existedDescr.ContainsKey(shortFileName))//Если у файла есть описание в descript.ion
                {
                    String[] fileKeywords = existedDescr[shortFileName].Split(new String[] { ",", ";", "\\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String keyWord in fileKeywords)//Проверяем, существует ли подкаталог с таким именем в рабочем каталоге. Если нет - создаем.
                    {
                        if (!Directory.Exists(Path.Combine(fullPath, keyWord)))
                        {
                            Directory.CreateDirectory(Path.Combine(fullPath, keyWord));
                        }
                        File.Copy(Path.Combine(fullPath, shortFileName), Path.Combine(fullPath, keyWord, shortFileName), overWriteWhenCopy);
                    }

                    if (applyToAll)//Проверяем, есть ли у текущего файла в ключевых словах заданное в качестве общего ключевое слово. Если нет - добавляем
                    {
                        if (Array.IndexOf(fileKeywords, applyTag) == -1)
                        {
                            existedDescr[shortFileName] = existedDescr[shortFileName] + ";" + applyTag;
                            if (!Directory.Exists(Path.Combine(fullPath, applyTag)))
                            {
                                Directory.CreateDirectory(Path.Combine(fullPath, applyTag));
                            }
                            File.Copy(Path.Combine(fullPath, shortFileName), Path.Combine(fullPath, applyTag, shortFileName), overWriteWhenCopy);
                        }
                    }
                }
                else//У файла нет описания. В этом случае проверяем, вызывалась ли программа с ключом applyToAll, и если да - применяем этот параметр к данному файлу
                {
                    if (applyToAll)
                    {
                        existedDescr.Add(shortFileName, applyTag);
                        if (!Directory.Exists(Path.Combine(fullPath, applyTag)))
                        {
                            Directory.CreateDirectory(Path.Combine(fullPath, applyTag));
                        }
                        File.Copy(Path.Combine(fullPath, shortFileName), Path.Combine(fullPath, applyTag, shortFileName), overWriteWhenCopy);
                    }
                }
            }

            //Убираем атрибут "скрытый" у файла descript.ion - без этого не получится перезаписать его
            File.SetAttributes(Path.Combine(fullPath, "descript.ion"), File.GetAttributes(Path.Combine(fullPath, "descript.ion")) & ~FileAttributes.Hidden);

            //Перезаписываем обновленный массив key-value в descript.ion
            StreamWriter sw = new StreamWriter(new FileStream(Path.Combine(fullPath, "descript.ion"), FileMode.Create), Encoding.GetEncoding(1251));
            foreach (KeyValuePair<String, String> item in existedDescr)
            {
                String fileName = item.Key;
                if (fileName.IndexOf(" ") != -1)
                {
                    fileName = "\"" + fileName + "\"";
                }
                sw.WriteLine(fileName + " " + item.Value);
            }
            sw.Close();
            
            //Выставляем атрибут "скрытый" обратно
            File.SetAttributes(Path.Combine(fullPath, "descript.ion"), File.GetAttributes(Path.Combine(fullPath, "descript.ion")) | FileAttributes.Hidden);
        }

        public static Dictionary<String, String> GetCurrentDescr(String pathToDir)
        {
            Dictionary<String, String> existedDescr = new Dictionary<string, string>();
            StreamReader sr = new StreamReader(new FileStream(Path.Combine(new String[] { pathToDir, "descript.ion" }), FileMode.Open), Encoding.GetEncoding(1251));//Файлы descript.ion хранятся в кодировке ANSI (кодовая страница 1251)

            while (!sr.EndOfStream)
            {
                String line = sr.ReadLine();
                if (line.Trim().Length == 0) continue;

                if (line.StartsWith("\""))//Файлы, содержащие в имени символ пробела, заключаются в кавычки
                {
                    String key = line.Substring(1, line.IndexOf("\"", 1) - 1);
                    String value = line.Substring(key.Length + 3);
                    existedDescr.Add(key, value);
                }
                else
                {
                    String key = line.Substring(0, line.IndexOf(" ", 1));
                    String value = line.Substring(key.Length + 1);
                    existedDescr.Add(key, value);
                }
            }
            sr.Close();

            return existedDescr;
        }
    }
}
